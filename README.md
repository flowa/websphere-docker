### Websphere Docker for Centos 7

Based on [http://veithen.github.io/2014/11/02/running-was-in-docker.html](this) so all kudos to Andreas Veithen

Basically you do this:

```
1. Install docker
2. get IBM installer (agent.installer thing) from here http://www-01.ibm.com/support/docview.wss

2. Clone this repo
3. set username and password for imutilsc parameters
    -userName
    - userPassword
    (These are you IBM credentials)
4. run 'sudo docker build [-t websphere/base]'
5. wait for one aeon for websphere to install
```

After installation you can run the container with command

```
sudo docker run -p 9060:9060 -p 9080:9080 -t websphere/base
```

-p options because that proxies needed management ports to host

Now you can use this as a base for further configuration

